
"""Convert all .ui (pyQt5) files into dir to .py .

Конвертируем все .ui файлы в каталоге в .py .
"""

# -*-coding: utf-8 -*-

import subprocess
from os import listdir
from os.path import isfile


for i in listdir():
    if isfile(i):
        # convert .ui (pyQt5) to .py
        if (i[-3:] == ".ui"):
            print(i)
            proc = subprocess.Popen("pyuic5 " + i + " -o " + i[:-3] + ".py -x")
    #
    # Для файлов ресурсов надо добавить
    # pyrcc4.exe -o …\View\MainWindow_rc.py -py3 …\View\MainWindow_rc.qrc
        # if (i[-6:] == "_rc.py"):
        #    print(i)
        #    proc = subprocess.Popen(/
        #   "pyrcc5.exe -o " + i + " -py5 " + i[:-3] + ".qrc")
